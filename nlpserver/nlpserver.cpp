#include "nlpserver.h"
#include "json.hpp"
#include "server.cpp"

#include <filesystem>
#include <fstream>
#include <iostream>

static const std::string HOME_DIR = std::getenv("HOME");
static const std::string USER_CONFIG_DIR =
    HOME_DIR + "/.config/kylin-ai/ondevice/";
static const std::string USER_CONFIG = USER_CONFIG_DIR + "nlpserver.json";
static const std::string SYSTEM_CONFIG =
    "/usr/share/kylin-ai/ondevice/nlpserver.json";
static const std::string DEFAULT_MODEL =
    "/usr/share/kylin-ai/ondevice/qwen1_5-4b-chat-q5_k_m.gguf";

static json compatCompletionParamParse(const struct llama_model *model,
                                       const std::string &input,
                                       const std::string &chatTemplate) {
    json body = json::parse(input);
    std::string message = format_chat(model, chatTemplate, body["messages"]);

    json params;
    params["prompt"] = message;
    params["cache_prompt"] = json_value(body, "cache_prompt", true);
    params["n_keep"] = json_value(body, "n_keep", 0);
    params["temperature"] = json_value(body, "temperature", 0.8f);
    params["top_k"] = json_value(body, "top_k", 40);
    params["top_p"] = json_value(body, "top_p", 0.95f);
    params["stream"] = json_value(body, "stream", true);

    return params;
}

static int lastUsedSlotId(server_context *ctx, std::vector<server_slot> &slots,
                          const json &prompt) {
    int lastSlotId = -1;
    int n_past = 10;
    std::vector<int> prompt_token =
        ctx->tokenize(prompt, ctx->system_prompt.empty() && ctx->add_bos_token);

    for (server_slot &slot : slots) {
        if (slot.cache_tokens.size() == 0) {
            continue;
        }

        int current_past = common_part(slot.cache_tokens, prompt_token);
        if (current_past > n_past) {
            n_past = current_past;
            lastSlotId = slot.id;
        }
    }

    return lastSlotId;
}

NlpServer::NlpServer()
    : ctxServer_(new server_context), params_(new gpt_params) {}

void NlpServer::run() {
    taskThread_ =
        std::thread([this]() { ctxServer_->queue_tasks.start_loop(); });
    taskThread_.detach();
    isRunning_ = true;
}

void NlpServer::terminate() {
    isRunning_ = false;
    ctxServer_->queue_tasks.terminate();
}

NlpServer &NlpServer::getInstance() {
    static NlpServer instance;
    return instance;
}

NlpServer::~NlpServer() {}

bool NlpServer::initialize() {
    loadParamFromConfig();
    llama_backend_init();
    llama_numa_init((*params_).numa);
    bool ret = ctxServer_->load_model(*params_);
    if (ret == false) {
        return false;
    }

    if (ctxServer_->validate_model_chat_template() == false) {
        chatTemplate_ = "chatml";
    }

    ctxServer_->init();
    ctxServer_->queue_tasks.on_new_task(
        [this](server_task task) { ctxServer_->process_single_task(task); });
    ctxServer_->queue_tasks.on_update_slots(
        [this]() { ctxServer_->update_slots(); });

    return true;
}

ai_engine::ErrorCode NlpServer::initSession(uint &sessionId) {
    if (ctxServer_ == nullptr) {
        return ai_engine::ErrorCode::ServiceError;
    }

    std::unique_lock<std::mutex> lock(sessionMutex_);

    static int id = 1;
    sessionId = id++;

    if (isRunning_) {
        return ai_engine::ErrorCode::Success;
    }

    if (!initialize()) {
        return ai_engine::ErrorCode::ModelError;
    }

    run();
    return ai_engine::ErrorCode::Success;
}

ai_engine::ErrorCode NlpServer::destroySession(uint sessionId) {
    std::unique_lock<std::mutex> lock(sessionMutex_);

    if (completionCallbacks_.find(sessionId) != completionCallbacks_.end()) {
        completionCallbacks_.erase(sessionId);
    }

    if (completionCallbacks_.size() != 0) {
        return ai_engine::ErrorCode::Success;
    }

    terminate();
    llama_backend_free();
    if (ctxServer_->ctx) {
        llama_free(ctxServer_->ctx);
        ctxServer_->ctx = nullptr;
    }
    if (ctxServer_->model) {
        llama_free_model(ctxServer_->model);
        ctxServer_->model = nullptr;
    }
    return ai_engine::ErrorCode::Success;
}

void NlpServer::setCompletionResultCallback(CompletionResultCallback callback,
                                            uint sessionId) {
    std::unique_lock<std::mutex> lock(sessionMutex_);

    completionCallbacks_[sessionId] = std::move(callback);
}

ai_engine::ErrorCode NlpServer::completion(const std::string &json_input,
                                           const uint sessionId, uint &task_id,
                                           int slot_id) {
    auto callback = completionCallbacks_[sessionId];
    if (callback == nullptr) {
        return ai_engine::ErrorCode::ComponentMissing;
    }

    // TODO chat_template not saved in server
    json data = compatCompletionParamParse(ctxServer_->model, json_input,
                                           chatTemplate_);

    const auto &prompt = data.find("prompt");

    if (slot_id == -1) {
        slot_id = lastUsedSlotId(ctxServer_.get(), ctxServer_->slots, *prompt);
    }
    data["id_slot"] = slot_id;

    task_id = getNewTaskID();
    addWaitingTaskID(task_id);
    ctxServer_->request_completion(task_id, -1, data, false, false);

    if (!json_value(data, "stream", false)) {
        return handleNonStreamResults(task_id, sessionId, callback);
    } else {
        return handleStreamResults(task_id, sessionId, callback);
    }
}

void NlpServer::cancelCompletion(const uint task_id) {
    server_task task;
    task.type = SERVER_TASK_TYPE_CANCEL;
    task.id_target = task_id;

    ctxServer_->queue_tasks.post(task);
}

int NlpServer::getNewTaskID() { return ctxServer_->queue_tasks.get_new_id(); }

void NlpServer::checkConfigFile() {
    if (std::filesystem::exists(USER_CONFIG)) {
        return;
    }
    if (!std::filesystem::exists(SYSTEM_CONFIG)) {
        return;
    }

    std::filesystem::create_directories(USER_CONFIG_DIR);
    std::filesystem::copy(SYSTEM_CONFIG, USER_CONFIG);
}

void NlpServer::loadParamFromConfig() {
    checkConfigFile();

    std::ifstream configuration(USER_CONFIG, std::ifstream::binary);
    if (!configuration.is_open()) {
        // 至少设置上 model 的路径，防止加载模型失败
        params_->model = DEFAULT_MODEL;
        return;
    }
    json params = json::parse(configuration);
    // clang-format off
    params_->n_threads      = json_value(params, "n_threads", get_num_physical_cores());
    params_->n_predict      = json_value(params, "n_predict", 256);
    params_->n_ctx          = json_value(params, "n_ctx", 2048);
    params_->n_batch        = json_value(params, "n_batch", 512);
    params_->n_keep         = json_value(params, "n_keep", 0);
    params_->n_parallel     = json_value(params, "n_parallel", 3);
    params_->n_gpu_layers   = json_value(params, "n_gpu_layers", 256);
    params_->model          = json_value(params, "model", DEFAULT_MODEL);
    params_->cont_batching  = json_value(params, "cont_batching", true);
    // clang-format on
}

void NlpServer::addWaitingTaskID(int task_id) {
    ctxServer_->queue_results.add_waiting_task_id(task_id);
}

void NlpServer::removeWaitingTaskID(int task_id) {
    ctxServer_->queue_results.remove_waiting_task_id(task_id);
}

ai_engine::ErrorCode NlpServer::handleStreamResults(
    int task_id, int sessionId,
    CompletionResultCallback completionResultCallback_) {
    bool resultError = false;
    while (true) {
        server_task_result result = receiveResult(task_id);
        result.data["sessionId"] = sessionId;
        const std::string str =
            result.data.dump(-1, ' ', false, json::error_handler_t::replace);
        completionResultCallback_(str);

        if (result.stop) {
            break;
        }
        if (result.error) {
            resultError = true;
            break;
        }
    }
    cancelCompletion(task_id);
    removeWaitingTaskID(task_id);

    if (resultError) {
        // TODO 这个返回值不一定合适，目前没有合适的感觉
        return ai_engine::ErrorCode::ResultBlocked;
    }
    return ai_engine::ErrorCode::Success;
}

ai_engine::ErrorCode NlpServer::handleNonStreamResults(
    int task_id, int sessionId,
    CompletionResultCallback completionResultCallback_) {
    server_task_result result = receiveResult(task_id);
    result.data["sessionId"] = sessionId;

    if (!result.error && result.stop) {
        completionResultCallback_(
            result.data.dump(-1, ' ', false, json::error_handler_t::replace));
    } else {
        completionResultCallback_(result.data["content"]);
    }
    removeWaitingTaskID(task_id);

    if (result.error) {
        return ai_engine::ErrorCode::ResultBlocked;
    }
    return ai_engine::ErrorCode::Success;
}

server_task_result NlpServer::receiveResult(int task_id) {
    return ctxServer_->queue_results.recv(task_id);
}
