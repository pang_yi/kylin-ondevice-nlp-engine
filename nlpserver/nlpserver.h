#pragma once

#include <kylin-ai/ai-engine/large-model/error.h>

#include <functional>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>

class server_context;
class server_task_result;
class gpt_params;

using CompletionResultCallback = std::function<void(const std::string &)>;

class NlpServer {
public:
    static NlpServer &getInstance();
    ~NlpServer();

    bool initialize();
    /**
     * @brief 初始化会话，返回会话ID
     * @param sessionId 会话ID
     */
    ai_engine::ErrorCode initSession(uint &sessionId);
    ai_engine::ErrorCode destroySession(uint sessionId);
    /**
     * @brief 设置文本扩写功能的回调函数
     *        调用此函数之前必须调用 initSession 函数初始化会话
     * @param callback  用于返回扩写结果的回调函数
     * @param sessionId 会话ID
     */
    void setCompletionResultCallback(CompletionResultCallback callback,
                                     uint sessionId);
    ai_engine::ErrorCode completion(const std::string &json_input,
                                    const uint sessionId, uint &task_id,
                                    int slot_id = -1);
    void cancelCompletion(const uint task_id);

private:
    NlpServer();
    void run();
    void terminate();
    int getNewTaskID();
    void checkConfigFile();
    void loadParamFromConfig();
    void addWaitingTaskID(int task_id);
    void removeWaitingTaskID(int task_id);
    ai_engine::ErrorCode handleStreamResults(int task_id, int sessionId,
                                             CompletionResultCallback callback);
    ai_engine::ErrorCode
    handleNonStreamResults(int task_id, int sessionId,
                           CompletionResultCallback callback);
    server_task_result receiveResult(int task_id);

private:
    bool isRunning_{false};
    std::thread taskThread_;
    std::mutex sessionMutex_;
    std::unique_ptr<server_context> ctxServer_;
    std::unique_ptr<gpt_params> params_;
    std::string chatTemplate_;
    std::unordered_map<int, CompletionResultCallback> completionCallbacks_;
};
