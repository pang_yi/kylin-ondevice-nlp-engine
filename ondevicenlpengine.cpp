#include "ondevicenlpengine.h"
#include "nlpserver/nlpserver.h"

#include <jsoncpp/json/json.h>

namespace ai_engine::lm::nlp {
OndeviceNlpEngine::OndeviceNlpEngine()
    : nlpServer_(&NlpServer::getInstance()) {}

OndeviceNlpEngine::~OndeviceNlpEngine() = default;

void OndeviceNlpEngine::setChatResultCallback(
    nlp::ChatResultCallback callback) {
    chatResultCallback_ = std::move(callback);

    if (nlpServer_ == nullptr) {
        return;
    }

    auto chatCallback = [this](const std::string &result) {
        this->onChatResult(result);
    };
    nlpServer_->setCompletionResultCallback(chatCallback, sessionId_);
}

void OndeviceNlpEngine::setContextSize(int size) { (void)size; }

void OndeviceNlpEngine::clearContext() {}

ErrorCode OndeviceNlpEngine::chat(const std::string &message) {
    if (nlpServer_ == nullptr) {
        return ErrorCode::ServiceError;
    }

    return nlpServer_->completion(message, sessionId_, taskId_, slotId_);
}

void OndeviceNlpEngine::stopChat() {
    if (nlpServer_ == nullptr) {
        return;
    }

    nlpServer_->cancelCompletion(taskId_);
}

void OndeviceNlpEngine::onChatResult(const std::string &result) {
    if (chatResultCallback_ == nullptr) {
        return;
    }

    Json::Reader reader;
    Json::Value body;
    if (!reader.parse(result, body)) {
        // std::cout << "Parse json failed !" << std::endl;
        return;
    }
    slotId_ = body["id_slot"].asUInt();

    Json::Value output;
    // output["sentence_id"] = 0; // 本地模型不需要 sentence_id
    output["is_end"] = body["stop"];
    output["result"] = body["content"];
    std::string realOutput = output.toStyledString();
    chatResultCallback_(result);
}

ErrorCode OndeviceNlpEngine::initChatModule() {
    if (inited_) {
        return ErrorCode::Success;
    }
    inited_ = true;
    return nlpServer_->initSession(sessionId_);
}

ErrorCode OndeviceNlpEngine::destroyChatModule() {
    if (!inited_) {
        return ErrorCode::Success;
    }
    inited_ = false;
    return nlpServer_->destroySession(sessionId_);
}

} // namespace ai_engine::lm::nlp
