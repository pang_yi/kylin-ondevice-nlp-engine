#include "ondevicenlpengine.h"

#include <iostream>

using namespace ai_engine::lm::nlp;

void printResult(const std::string &result) {
    std::cout << result << std::endl;
}

void engine2PrintResult(const std::string &result) {
    std::cout << "Engine2: " << result << std::endl;
}

int main() {
    OndeviceNlpEngine engine;
    engine.initChatModule();
    engine.setChatResultCallback(printResult);
    engine.chat(
        "{\"messages\": [{\"role\": \"user\", \"content\": \"你是谁？\"}]}");

    OndeviceNlpEngine engine2;
    engine2.initChatModule();
    engine2.setChatResultCallback(engine2PrintResult);
    engine2.chat(
        "{\"messages\": [{\"role\": \"user\", \"content\": \"你好\"}]}");

    engine.chat("{\"messages\": [{\"role\": \"user\", \"content\": "
                "\"今天星期几？\"}]}");

    while (true) {
        ;
    }
    return 0;
}