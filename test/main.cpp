#include <iostream>
#include "nlpserver.h"
#include "httplib.h"
#include "json.hpp"
#include "utils.hpp"

#include <condition_variable>
#include <mutex>
#include <thread>
#include <vector>

static std::vector<std::string> answers;
std::mutex mutex_answers;
std::condition_variable condition_answers;

int main(int argc, char **argv) {
    NlpServer &server = NlpServer::getInstance();
    server.start();

    std::unique_ptr<httplib::Server> svr;
    svr.reset(new httplib::Server());
    svr->set_default_headers({{"NLP", "ai-subsystem"}});

    // set timeouts and change hostname and port
    svr->set_read_timeout(600);
    svr->set_write_timeout(600);

    if (!svr->bind_to_port("127.0.0.1", 8080)) {
        fprintf(stderr,
                "\ncouldn't bind to server socket: hostname=%s port=%d\n\n",
                "127.0.0.1", 8080);
        return 1;
    }

    const auto handle_completions = [&server](const httplib::Request &req,
                                              httplib::Response &res) {
        res.set_header("Access-Control-Allow-Origin",
                       req.get_header_value("Origin"));

        bool stream = true;
        json body = json::parse(req.body);
        stream = json_value(body, "stream", true);

        const auto printAnswer = [&res, stream](const std::string &answer) {
            if (!stream) {
                res.set_content(answer, "application/json; charset=utf-8");
                return;
            }
            std::unique_lock<std::mutex> lock(mutex_answers);
            answers.push_back(std::move(answer));
            condition_answers.notify_all();
        };

        const auto chunked_content_provider = [](size_t,
                                                 httplib::DataSink &sink) {
            while (true) {
                std::unique_lock<std::mutex> lock(mutex_answers);
                condition_answers.wait(lock,
                                       [&] { return (!answers.empty()); });
                std::string answer = answers.front();
                answers.erase(answers.begin());

                json json_answer = json::parse(answer);

                answer = "data: " + answer;
                answer += "\n\n";
                if (!sink.write(answer.c_str(), answer.size())) {
                    std::cout << "sink wrinting error" << std::endl;
                    return false;
                }
                if (json_answer.contains("stop") &&
                    json_answer["stop"] == true) {
                    break;
                }
            }
            sink.done();
            return true;
        };

        auto on_complete = [](bool) {
            std::cout << "on_complete" << std::endl;
        };

        int sessionId;
        server.setCompletionResultCallback(printAnswer, sessionId);

        res.set_chunked_content_provider("text/event-stream",
                                         chunked_content_provider, on_complete);

        std::thread t([&] {
            uint task_id;
            server.completion(req.body, sessionId, task_id);
        });
        t.detach();
    };

    svr->Post("/v1/chat/completions", handle_completions);

    svr->new_task_queue = [] { return new httplib::ThreadPool(8); };

    std::thread t([&]() {
        if (!svr->listen_after_bind()) {
            return 1;
        }

        return 0;
    });

    t.join();
    std::cout << "Hello world ! ! !" << std::endl;
    return 0;
}