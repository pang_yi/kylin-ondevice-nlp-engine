#pragma once

#include <string>
#include <kylin-ai/ai-engine/large-model/endsidenlpengine.h>

class NlpServer;

namespace ai_engine::lm::nlp {

class OndeviceNlpEngine : public AbstractEndSideNlpEngine {
public:
    OndeviceNlpEngine();
    ~OndeviceNlpEngine();
    std::string engineName() override { return "ondevice-nlp"; }
    void setChatResultCallback(ChatResultCallback callback) override;
    void setContextSize(int size) override;
    void clearContext() override;
    ErrorCode chat(const std::string &message) override;
    void stopChat() override;

    void onChatResult(const std::string &result);

    ai_engine::ErrorCode initChatModule() override;
    ai_engine::ErrorCode destroyChatModule() override;

private:
    uint taskId_ = 0;
    int slotId_ = -1;
    uint sessionId_ = 0;
    bool inited_ = false;
    NlpServer *nlpServer_ = nullptr;
    ChatResultCallback chatResultCallback_ = nullptr;
};

} // namespace ai_engine::lm::nlp
